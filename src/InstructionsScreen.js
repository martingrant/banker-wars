/**
 * Created by Marty on 04/05/2014.
 */

/**
 * Created by Marty on 30/04/2014.
 */

var music;

InstructionsScreen = function() {};
InstructionsScreen.prototype = {

    preload: function() {

        // Load images
        this.load.image('background', 'assets/images/background.png');
        this.load.image('menuButton', 'assets/images/menubutton.png');
        this.load.image('ground', 'assets/images/ground.png');
        this.load.image('soundButton', 'assets/images/soundbutton.png');

        // Load audio
        this.load.audio('music', 'assets/audio/music.ogg', 1, true);

        // Create leaderboard
        leaderboard = new Clay.Leaderboard( { id: 3825 } );

    },
    create:  function() {

        // Create background image sprite
        bankerWars.background = bankerWars.add.sprite(0, 0, 'background');

        // Create ground image sprite
        bankerWars.ground = bankerWars.add.sprite(0, 550, 'ground');

        // Create buttons
        bankerWars.menuButton = bankerWars.add.button(310, 450, "menuButton", switchToMenuScreen, this);
        bankerWars.soundButton = bankerWars.add.button(720, 5, "soundButton", toggleInstructionsScreenSound, this);

        // Create and play music
        bankerWars.music = bankerWars.add.audio('music');
        music.play('', 0, 1, true);

        // Create instructions text
        var titleStyle = { font: "40px Arial", fill: "#ff0044", stroke: "#000000", strokeThickness: 6, align: "center" };
        bankerWars.instructionsTitle = bankerWars.add.text(390, 40, "Instructions", titleStyle);
        bankerWars.instructionsTitle.setShadow(5, 5, 'rgba(0,0,0,0.5)', 15);
        bankerWars.instructionsTitle.anchor.set(0.5);

        var paragraphStyle = { font: "20px Arial", fill: "#ff0044", stroke: "#000000", strokeThickness: 6, align: "center" };
        bankerWars.instructionsText = bankerWars.add.text(390, 250,
            "You are taking on the roll of a greedy banker.\n You are to use your cannon to propel packages of debt towards the financial system.\n\n " +
                "Pull your finger back from the cannon and let go to release a debt package.\n You can change how high or low the cannon will fire.\n\n" +
                "You have only 10 debt packages to use.\n Different parts of the financial system will rack up more debt.\n When you have no more debt packages left,\n you can submit your score to the leaderboard.",
            paragraphStyle);
        bankerWars.instructionsText.anchor.set(0.5);


    },
    update:  function() {

    }
};


// Function to switch to MenuScreen
var switchToMenuScreen = function() {
    music.stop();
    bankerWars.state.start('MenuScreen', true, true);
};

// Function to toggle sound
var toggleInstructionsScreenSound = function() {
    if (music.volume == 1)
        music.volume = 0;
    else
        music.volume = 1;
};


