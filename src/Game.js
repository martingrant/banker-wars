/**
 * Created with JetBrains WebStorm.
 * User: b00228322
 * Date: 24/04/14
 * Time: 19:07
 * To change this template use File | Settings | File Templates.
 */

var bankerWars = new Phaser.Game(800, 600, Phaser.AUTO, '');

var MenuScreen = {};
var GameScreen = {};
var InstructionsScreen = {};
var CreditsScreen = {};

window.onload = function() {

    bankerWars.state.add("MenuScreen", MenuScreen);
    bankerWars.state.add("GameScreen", GameScreen);
    bankerWars.state.add("InstructionsScreen", InstructionsScreen);
    bankerWars.state.add("CreditsScreen", CreditsScreen);

    bankerWars.state.start("MenuScreen", true, true);
};