/**
 * Created by MARTIN on 05/05/14.
 */
/**
 * Created by Marty on 04/05/2014.
 */

/**
 * Created by Marty on 30/04/2014.
 */

var music;

CreditsScreen = function() {};
CreditsScreen.prototype = {

    preload: function() {

        // Load images
        this.load.image('background', 'assets/images/background.png');
        this.load.image('menuButton', 'assets/images/menubutton.png');
        this.load.image('ground', 'assets/images/ground.png');
        this.load.image('soundButton', 'assets/images/soundbutton.png');

        // Load audio
        this.load.audio('music', 'assets/audio/music.ogg', 1, true);

        // Create leaderboard
        leaderboard = new Clay.Leaderboard( { id: 3825 } );

    },
    create:  function() {

        // Create background image sprite
        bankerWars.background = bankerWars.add.sprite(0, 0, 'background');

        // Create ground image sprite
        bankerWars.ground = bankerWars.add.sprite(0, 550, 'ground');

        // Create buttons
        bankerWars.menuButton = bankerWars.add.button(310, 500, "menuButton", switchToMenuScreen, this);
        bankerWars.soundButton = bankerWars.add.button(720, 5, "soundButton", toggleCreditsScreenSound, this);

        // Create and play music
        bankerWars.music = bankerWars.add.audio('music');
        music.play('', 0, 1, true);

        // Create instructions text
        var titleStyle = { font: "40px Arial", fill: "#ff0044", stroke: "#000000", strokeThickness: 6, align: "center" };
        bankerWars.creditsTitle = bankerWars.add.text(390, 40, "Credits", titleStyle);
        bankerWars.creditsTitle.setShadow(5, 5, 'rgba(0,0,0,0.5)', 15);
        bankerWars.creditsTitle.anchor.set(0.5);

        var paragraphStyle = { font: "20px Arial", fill: "#ff0044", stroke: "#000000", strokeThickness: 6, align: "center" };
        bankerWars.creditsText = bankerWars.add.text(390, 270,
            "Created by:\n Steven Gordon & Martin Grant\n\n " +
                "Developed using HTML5 and Javascript with Phaser.\n\n www.midnightpacific.com, @_martingrant\n\n" +
                "steve_gordon@live.co.uk\n\n Audio:\n Hidden Agenda by Kevin MacLeod (incompetech.com)\n light_crate_smash1.wav by https://www.freesound.org/people/Srehpog/\n Firework 1 by http://www.freesfx.co.uk/users/stuartduffield/",
            paragraphStyle);
        bankerWars.creditsText.anchor.set(0.5);


    },
    update:  function() {

    }
};


// Function to switch to MenuScreen
var switchToMenuScreen = function() {
    music.stop();
    bankerWars.state.start('MenuScreen', true, true);
};

// Function to toggle sound
var toggleCreditsScreenSound = function() {
    if (music.volume == 1)
        music.volume = 0;
    else
        music.volume = 1;
};


