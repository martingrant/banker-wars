/**
 * Created by Marty on 30/04/2014.
 */

var music;

var leaderboard;

MenuScreen = function() {};
MenuScreen.prototype = {

    preload: function() {

        // Load images
        this.load.image('logo', 'assets/images/gamelogo.png');
        this.load.image('background', 'assets/images/background.png');
        this.load.image('startButton', 'assets/images/startbutton.png');
        this.load.image('highScoreButton', 'assets/images/highscorebutton.png');
        this.load.image('instructionsButton', 'assets/images/instructionsbutton.png');
        this.load.image('ground', 'assets/images/ground.png');
        this.load.image('soundButton', 'assets/images/soundbutton.png');
        this.load.image('creditsButton', 'assets/images/creditsbutton.png');

        // Load audio
        this.load.audio('music', 'assets/audio/music.ogg', 1, true);

        // Create leaderboard
        Clay.ready( function () {
            leaderboard = new Clay.Leaderboard( { id: 3825 } );
        } );

    },
    create:  function() {

        // Create background image sprite
        var background = this.add.sprite(0, 0, 'background');
        background.fixedToCamera = true;

        // Create game logo image sprite
        var logo = this.add.sprite(90, 30, 'logo');
        logo.fixedToCamera = true;

        // Create ground image sprite
        var ground = this.add.sprite(0, 550, 'ground');

        // Create buttons
        var startButton = this.add.button(310, 200, "startButton", switchToGameScreen, this);
        var highScoreButton = this.add.button(310, 300, "highScoreButton", showLeaderboard, this);
        var instructionsButton = this.add.button(308, 400, "instructionsButton", switchToInstructionsScreen, this);
        var soundButton = this.add.button(720, 5, "soundButton", toggleStartScreenSound, this);
        var creditsButton = this.add.button(310, 500, "creditsButton", switchToCreditsScreen, this);

        // Create and play music
        music = this.add.audio('music');
        music.play('', 0, 1, true);

    },
    update:  function() {

    }
};

// Function to switch to GameScreen
var switchToGameScreen = function() {
    music.stop();
    bankerWars.state.start('GameScreen', true, true);
};


// Function to switch to InstructionsScreen
var switchToInstructionsScreen = function() {
    music.stop();
    bankerWars.state.start('InstructionsScreen', true, true);
};


// Function to switch to CreditsScreen
var switchToCreditsScreen = function() {
    music.stop();
    bankerWars.state.start('CreditsScreen', true, true);
};


// Function to show leaderboard
var showLeaderboard = function() {
    // Set options for leaderboard
    var options = {
        sort: 'desc',
        filter: ['day', 'month'],
        cumulative: false,
        best: false,
        limit: 10,
        self: false,
        friends: false,
        showPersonal: true
    };
    var callback = function( response ) {
        console.log( response );
    };
    // Display the leaderboard
    leaderboard.show( options, callback );

};

// Function to toggle sound
var toggleStartScreenSound = function() {
    if (music.volume == 1)
        music.volume = 0;
    else
        music.volume = 1;
};

