/**
 * Created by Marty on 30/04/2014.
 */

GameScreen = function() {};

var player1StartX = 300;
var player1StartY = 900;
var points = 0;
var projectileCount = 10; // change back to 5

var testTouchingString;

GameScreen.prototype = {

    preload: function () {

        bankerWars.load.image('background', 'assets/images/backgroundscaled.png');
        bankerWars.load.image('bomb', 'assets/images/debtprojectile.png');
        bankerWars.load.image('CannonBarrel', 'assets/images/cannonbarrel.png');
        bankerWars.load.image('CannonBase', 'assets/images/cannonbase.png');
        bankerWars.load.image('ground', 'assets/images/ground.png');
        bankerWars.load.image('exitButton', 'assets/images/exitbutton.png');
        bankerWars.load.image('soundButton', 'assets/images/soundbutton.png');
        bankerWars.load.image('submitScoreButton', 'assets/images/submitscorebutton.png');
        bankerWars.load.image('menuButton', 'assets/images/menubutton.png');

        bankerWars.load.image('stoneWindow', 'assets/images/window.png');
        bankerWars.load.image('glass', 'assets/images/glass.png');
        bankerWars.load.image('door', 'assets/images/door.png');
        bankerWars.load.image('concrete', 'assets/images/concrete.png');
        bankerWars.load.image('girder', 'assets/images/girder.png');

        bankerWars.load.audio('cannon', 'assets/audio/cannon.mp3', 1, false);
        bankerWars.load.audio('smash', 'assets/audio/smash.ogg', 1, false);
    },

    create: function () {
        // Set game world size
        bankerWars.world.setBounds(0, 0, 1600, 1200);
        bankerWars.add.sprite(0, 0, 'background');

        // The Cannon's Barrel
        bankerWars.CannonBarrel = bankerWars.add.sprite(player1StartX, player1StartY, 'CannonBarrel');
        bankerWars.CannonBarrel.anchor.setTo(0.1, 0.5);

        // The Cannon's base
        bankerWars.CannonBase = bankerWars.add.sprite(player1StartX, player1StartY, 'CannonBase');
        bankerWars.CannonBase.anchor.setTo(0.75, 0.4);

        // Projectile
        bankerWars.projectile = bankerWars.add.sprite(player1StartX, player1StartY, 'bomb');
        bankerWars.projectile.anchor.setTo(0.5, 0.5);
        bankerWars.projectile.alpha = 0;

        // Enable physics
        bankerWars.physics.enable(bankerWars.projectile, Phaser.Physics.ARCADE);

        // Set projectile physics
        bankerWars.projectile.body.bounce.set(0.1, 0.25);
        bankerWars.projectile.body.drag.set(50, 0);
        bankerWars.projectile.body.mass = 10;

        // Input
        bankerWars.projectile.inputEnabled = true;
        bankerWars.projectile.input.start(0, true);
        bankerWars.projectile.events.onInputDown.add(this.set);
        bankerWars.projectile.events.onInputUp.add(this.launch);

        bankerWars.camera.follow(bankerWars.projectile, Phaser.Camera.FOLLOW_TOPDOWN);  // sets camera to follow the projectile

        bankerWars.cannonSound = bankerWars.add.audio('cannon');

        // Create ground image sprite
        bankerWars.ground = bankerWars.add.tileSprite(0, this.world.height, 1600, 50, 'ground');
        bankerWars.ground.anchor.setTo(0,1);
        // Enable physics and set to immovable
        bankerWars.physics.enable(bankerWars.ground, Phaser.Physics.ARCADE);
        bankerWars.ground.body.collideWorldBounds = true;
        bankerWars.ground.body.immovable = true;

        // Collidable objects
        // Create collidable group and enable its physics
        bankerWars.collidables = bankerWars.add.group();
        bankerWars.collidables.enableBody = true;
        bankerWars.collidables.physicsBodyType = Phaser.Physics.ARCADE;

        // Add collidables to collidables group
        var door = bankerWars.collidables.create(1250, 1150, 'door');

        var concrete = bankerWars.collidables.create(1150, 1150, 'concrete');
        concrete = bankerWars.collidables.create(1150, 1050, 'concrete');
        concrete = bankerWars.collidables.create(1250, 1050, 'concrete');
        concrete = bankerWars.collidables.create(1350, 1050, 'concrete');
        concrete = bankerWars.collidables.create(1350, 1150, 'concrete');

        var girder = bankerWars.collidables.create(1100, 1150, 'girder');
        girder = bankerWars.collidables.create(1100, 1000, 'girder');
        girder = bankerWars.collidables.create(1100, 850, 'girder');
        girder = bankerWars.collidables.create(1100, 700, 'girder');
        girder = bankerWars.collidables.create(1200, 600, 'girder');
        girder = bankerWars.collidables.create(1200, 450, 'girder');
        girder = bankerWars.collidables.create(1350, 600, 'girder');
        girder = bankerWars.collidables.create(1350, 450, 'girder');
        girder = bankerWars.collidables.create(1450, 1150, 'girder');
        girder = bankerWars.collidables.create(1450, 1000, 'girder');
        girder = bankerWars.collidables.create(1450, 850, 'girder');
        girder = bankerWars.collidables.create(1450, 700, 'girder');

        var glass = bankerWars.collidables.create(1150, 950, 'glass');
        glass = bankerWars.collidables.create(1150, 900, 'glass');
        glass = bankerWars.collidables.create(1150, 850, 'glass');
        glass = bankerWars.collidables.create(1150, 800, 'glass');
        glass = bankerWars.collidables.create(1150, 750, 'glass');
        glass = bankerWars.collidables.create(1150, 700, 'glass');
        glass = bankerWars.collidables.create(1150, 650, 'glass');
        glass = bankerWars.collidables.create(1300, 950, 'glass');
        glass = bankerWars.collidables.create(1300, 900, 'glass');
        glass = bankerWars.collidables.create(1300, 850, 'glass');
        glass = bankerWars.collidables.create(1300, 800, 'glass');
        glass = bankerWars.collidables.create(1300, 750, 'glass');
        glass = bankerWars.collidables.create(1300, 700, 'glass');
        glass = bankerWars.collidables.create(1300, 650, 'glass');

        var stoneWindow = bankerWars.collidables.create(1250, 600, 'stoneWindow');
        stoneWindow = bankerWars.collidables.create(1250, 550, 'stoneWindow');
        stoneWindow = bankerWars.collidables.create(1250, 500, 'stoneWindow');
        stoneWindow = bankerWars.collidables.create(1250, 450, 'stoneWindow');
        stoneWindow = bankerWars.collidables.create(1250, 400, 'stoneWindow');
        stoneWindow = bankerWars.collidables.create(1250, 350, 'stoneWindow');
        stoneWindow = bankerWars.collidables.create(1300, 600, 'stoneWindow');
        stoneWindow = bankerWars.collidables.create(1300, 550, 'stoneWindow');
        stoneWindow = bankerWars.collidables.create(1300, 500, 'stoneWindow');
        stoneWindow = bankerWars.collidables.create(1300, 450, 'stoneWindow');
        stoneWindow = bankerWars.collidables.create(1300, 400, 'stoneWindow');
        stoneWindow = bankerWars.collidables.create(1300, 350, 'stoneWindow');

        // Set collidable up physics
        bankerWars.collidables.forEach(this.setCollidablePhysics, this);

        // Create text to display projectileCount
        var style = { font: "30px Arial", fill: "#ff0044", align: "center" };
        bankerWars.projectileCountText = bankerWars.add.text(250, 30, "Debt Packages: " + projectileCount, style);
        bankerWars.projectileCountText.setShadow(5, 5, 'rgba(0,0,0,0.5)', 15);
        bankerWars.projectileCountText.anchor.set(0.5);
        bankerWars.projectileCountText.fixedToCamera = true;

        // Create text to display scores
        bankerWars.pointsText = bankerWars.add.text(500, 30, "Debt: $" + points, style);
        bankerWars.pointsText.setShadow(5, 5, 'rgba(0,0,0,0.5)', 15);
        bankerWars.pointsText.anchor.set(0.5);
        bankerWars.pointsText.fixedToCamera = true;

        // Cannon platform
        bankerWars.cannonPlatform = this.add.tileSprite(player1StartX-100, player1StartY+30, 200, 225, 'concrete');

        // Exit Button
        bankerWars.exitButton = bankerWars.add.button(15, 0, "exitButton", switchToMenuScreen, this);
        bankerWars.exitButton.fixedToCamera = true;

        // Sound Button
        bankerWars.soundButton = bankerWars.add.button(720, 0, "soundButton", toggleGameScreenSound, this);
        bankerWars.soundButton.fixedToCamera = true;

        // Submit score button
        bankerWars.submitScoreButton = bankerWars.add.button(310, 2000, "submitScoreButton", submitScore, this);
        bankerWars.submitScoreButton.inputEnabled = false;

        // Menu button
        bankerWars.menuButton = bankerWars.add.button(310, 2000, "menuButton", switchToMenuScreen, this);
        bankerWars.menuButton.inputEnabled = false;

        bankerWars.smashSound = bankerWars.add.audio('smash');

        points = 0;
        projectileCount = 10;
    },

    setCollidablePhysics: function(collidable) {
        collidable.anchor.setTo(0, 1);
        collidable.body.bounce.set(0.1, 0.001);
        collidable.body.drag.set(50, 50);
        collidable.body.gravity.setTo(0, 200);
        collidable.body.allowGravity = false;
        collidable.body.immovable = true;
        collidable.body.mass = ( (collidable.width / 10) * (collidable.height / 10) );
        // Set Points and health
        collidable.totalHealth = ( (collidable.width / 50) * (collidable.height / 50) );
        collidable.health = collidable.totalHealth;
        collidable.pointsOnHit = ( (collidable.width * 2) * (collidable.height) );
        collidable.pointsOnDestroy = (collidable.health * collidable.pointsOnHit);
    },

    update: function () {
        if(projectileCount != 0 /*|| bankerWars.collidables.countDead()*/)
        {
            bankerWars.projectileCountText.setText("Debt Package: " + projectileCount);
            bankerWars.pointsText.setText("Debt: $" + points);

            // Collisions for collidable objects
            bankerWars.physics.arcade.collide(bankerWars.ground, bankerWars.projectile);
            bankerWars.physics.arcade.collideSpriteVsGroup(bankerWars.projectile, bankerWars.collidables, this.projectileCollisionResolver);
            bankerWars.physics.arcade.collide(bankerWars.ground, bankerWars.collidables);
            bankerWars.physics.arcade.collideGroupVsSelf(bankerWars.collidables);

            if(!bankerWars.launched)
            {
                var inputPosition = new Phaser.Point(bankerWars.input.activePointer.worldX, bankerWars.input.activePointer.worldY);
                bankerWars.CannonBarrel.rotation = bankerWars.physics.arcade.angleBetween(inputPosition, bankerWars.CannonBarrel);

                if(bankerWars.CannonBarrel.rotation < -1.4 ) // -80 degrees (up is -ve rotation)
                {
                    bankerWars.CannonBarrel.rotation = -1.4;
                }
                else if(bankerWars.CannonBarrel.rotation > 0.18) // 10 degrees (down is +ve rotation)
                {
                    bankerWars.CannonBarrel.rotation = 0.18;
                }
            }

            if(bankerWars.catchFlag)
            {
                bankerWars.projectile.x = bankerWars.input.activePointer.worldX;
                bankerWars.projectile.y = bankerWars.input.activePointer.worldY;

               if(bankerWars.CannonBarrel.rotation < -1.4 ) // -80 degrees (up is -ve rotation)
               {
                    bankerWars.CannonBarrel.rotation = -1.4;
               }
               else if(bankerWars.CannonBarrel.rotation > 0.18) // 10 degrees (down is +ve rotation)
               {
                   bankerWars.CannonBarrel.rotation = 0.18;
               }
            }

            if(bankerWars.launched)
            {
                bankerWars.projectile.body.gravity.setTo(0, 200);
                bankerWars.projectile.body.allowGravity = true;

                if ( bankerWars.projectile.x > bankerWars.world.width ||
                    (bankerWars.projectile.body.deltaAbsX() < 0.5 && bankerWars.projectile.body.deltaAbsY() < 0.5))
                {
                    this.reset();
                }
            }
        }
        else
        {
            endGame();
        }
    },

    reset: function()
    {
        bankerWars.launched = false;

        bankerWars.projectile.alpha = 0;
        bankerWars.projectile.body.velocity.setTo(0, 0);
        bankerWars.projectile.body.moves = false;
        bankerWars.projectile.x = player1StartX;
        bankerWars.projectile.y = player1StartY;

        bankerWars.camera.follow(bankerWars.CannonBarrel, Phaser.Camera.FOLLOW_TOPDOWN);
        --projectileCount;
    },

    set: function(projectile, pointer)
    {
        if (!bankerWars.launched)
        {
            bankerWars.catchFlag = true;
            bankerWars.camera.follow(null);
            projectile.body.moves = false;
            projectile.body.gravity.set(0);
            projectile.body.velocity.set(0);
        }
    },

    launch: function()
    {
        if (bankerWars.catchFlag)
        {
            bankerWars.projectile.alpha = 1;

            bankerWars.cannonSound.play();

            bankerWars.catchFlag = false;
            bankerWars.launched = true;

            var inputPosition = new Phaser.Point(bankerWars.input.activePointer.worldX, bankerWars.input.activePointer.worldY);
            var theta = bankerWars.physics.arcade.angleBetween(inputPosition, bankerWars.CannonBarrel);

            var distanceX;
            var distanceY;
            var distance = bankerWars.physics.arcade.distanceBetween(bankerWars.CannonBarrel, inputPosition);

            // Govern the distance the sprite is dragged away from launch post
            if(distance > 200)
            {
                distance = (200.0 / distance) * distance;
            }

            if(theta < -1.4 )
            {
                theta = -1.4;
            }
            else if(theta > 0.18)
            {
                theta = 0.18;
            }

            distanceX = Math.cos(theta) * distance;
            distanceY = Math.sin(theta) * distance;

            bankerWars.projectile.x = distanceX;
            bankerWars.projectile.y = distanceY;

            bankerWars.camera.follow(bankerWars.projectile, Phaser.Camera.FOLLOW_TOPDOWN);

            var xPower = distanceX * 3;
            var yPower = distanceY * 3;

            var adjacentX = Math.cos(theta) * bankerWars.CannonBarrel.width;
            var oppositeY = Math.sin(theta) * bankerWars.CannonBarrel.width;

            bankerWars.projectile.x = player1StartX + adjacentX;
            bankerWars.projectile.y = player1StartY + oppositeY;

            bankerWars.projectile.body.moves = true;
            bankerWars.projectile.body.gravity.setTo(0, 200);
            bankerWars.projectile.body.velocity.setTo(xPower, yPower);
        }
    },

    projectileCollisionResolver: function(projectile, collidable) {
        var projectileForce = projectile.body.speed / collidable.body.mass;
        var projectileXVelocity;
        var projectileYVelocity;

        if(projectileForce > collidable.health)
        {
            points =  points + collidable.pointsOnDestroy;
            collidable.kill();
            bankerWars.smashSound.play();

            var scalar = (projectileForce - collidable.health) * 0.25;

            if(projectile.body.velocity.x < 0)
            {
                projectileXVelocity = -1 * (projectile.body.velocity.x * scalar);
            }
            else
            {
                projectileXVelocity = projectile.body.velocity.x * scalar;
            }
            projectileYVelocity = projectile.body.velocity.y;

            projectile.body.velocity.setTo(projectileXVelocity, projectileYVelocity);
        }
        else if(projectileForce >= 1)
        {
            collidable.health -= Math.floor(projectileForce);

            var hitPointsLost = collidable.totalHealth - collidable.health;

            points =  points + (collidable.pointsOnHit * hitPointsLost);

            projectileXVelocity = (projectile.body.velocity.x * projectile.body.bounce.x);
            projectileYVelocity = (projectile.body.velocity.y * projectile.body.bounce.y);

            projectile.body.velocity.setTo(projectileXVelocity, projectileYVelocity);
        }
    }
};

// Function to switch to MenuScreen
var switchToMenuScreen = function() {
    bankerWars.state.start('MenuScreen', true, true);
};

// Function to toggle sound
var toggleGameScreenSound = function() {
    if (bankerWars.cannonSound.volume == 1)
        bankerWars.cannonSound.volume = 0;
    else
        bankerWars.cannonSound.volume = 1;
};


// Function to be called once game is over
var endGame = function() {
    bankerWars.submitScoreButton.y = 750;
    bankerWars.submitScoreButton.inputEnabled = true;

    bankerWars.menuButton.y = 850;
    bankerWars.menuButton.inputEnabled = true;
};


// Function to submit score
var submitScore = function() {
    Clay.ready( function () {
        leaderboard.post( { score: points }, function( response ) {
            console.log( response );
            switchToMenuScreen();
        } );
    } );
};